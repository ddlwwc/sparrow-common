package xin.sparrow.cache.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wancheng on 2019/9/27.
 */
@Slf4j
@Component
public class RedisBase {

    @Autowired(required = false)
    private JedisPool jedisPool;

    private void close(Jedis jedis) {
        try {
            if (jedis != null) {
                jedis.close();
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }
    public JedisPool getJedisPool(){
        return jedisPool;
    }
    public String get(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.get(key);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return null;
    }
    public void transactional(){
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Transaction transaction = jedis.multi();
            transaction.set("111111", "1111111");
            transaction.set("sssdddds", "ddd");
            transaction.exec();
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }
    public void set(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.set(key, value);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }

    public String hget(String redisKey, String hkey) {
        Jedis jedis = null;
        String res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.hget(redisKey, hkey);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }

    public void hset(String redisKey, String hkey, String hvalue) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.hset(redisKey, hkey, hvalue);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }

    public void hmset(String redisKey, Map<String, String> map) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.hmset(redisKey, map);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }

    public List<String> hget(String redisKey, String... hkey) {
        Jedis jedis = null;
        List<String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.hmget(redisKey, hkey);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }

    public Map<String, String> hgetAll(String redisKey) {
        Jedis jedis = null;
        Map<String, String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.hgetAll(redisKey);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }

    public List<String> lget(String redisKey, Long start, Long end) {
        Jedis jedis = null;
        List<String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.lrange(redisKey, start, end);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;

    }

    public Set<String> zget(String redisKey, Long start, Long end) {
        Jedis jedis = null;
        Set<String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.zrange(redisKey, start, end);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }
}
