package xin.sparrow.cache.redis;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wancheng on 2019/7/18.
 */
@Data
@Accessors(chain = true)
@Slf4j
public class RedisInstance {
    /**
     * 静态内部类
     */
    private static class LazyHolder {
        private static final RedisInstance INSTANCE = new RedisInstance();
    }

    /**
     * 共有的获取方法
     *
     * @return
     */
    public static RedisInstance getInstance() {
        return LazyHolder.INSTANCE;
    }

    private RedisInstance() {
        this.init();
    }

    private void init() {
        try {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(maxTotal);
            jedisPoolConfig.setMaxIdle(maxIdle);
            jedisPoolConfig.setMaxWaitMillis(maxWait);
            jedisPoolConfig.setTestOnBorrow(testOnBorrow);
            jedisPoolConfig.setTestOnCreate(testOnCreate);
            jedisPoolConfig.setTestOnReturn(testOnReturn);
            jedisPoolConfig.setTestWhileIdle(testWhileIdle);
            jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password, database);
        } catch (Exception e) {
            log.error("初始化 Redis 链接异常: {}", e.getMessage());

        }
    }

    /**
     * jedis连接池
     */
    private JedisPool jedisPool;
    /**
     * ip 默认本机地址
     */
    public static String host = "127.0.0.1";
    /**
     * 端口 默认6379
     */
    public static int port = 6379;
    /**
     * 最大链接数
     */
    public static int maxTotal = 2048;
    /**
     * 最小链接数
     */
    public static int maxIdle = 200;
    /**
     * 等待时间
     */
    public static int maxWait = 10000;
    /**
     * 数据库 默认0
     */
    public static int database = 0;
    /**
     * 超时时间
     */
    public static int timeout = 2000;
    /**
     * 密码
     */
    public static String password = null;
    /**
     *
     */
    public static boolean testOnCreate = false;
    /**
     *
     */
    public static boolean testOnBorrow = false;
    /**
     *
     */
    public static boolean testOnReturn = false;
    /**
     *
     */
    public static boolean testWhileIdle = false;

    private void close(Jedis jedis) {
        try {
            if (jedis != null) {
                jedis.close();
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    public String get(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.get(key);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return null;
    }

    public void set(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.set(key, value);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }

    public String hget(String redisKey, String hkey) {
        Jedis jedis = null;
        String res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.hget(redisKey, hkey);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }

    public void hset(String redisKey, String hkey, String hvalue) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.hset(redisKey, hkey, hvalue);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }

    public void hmset(String redisKey, Map<String, String> map) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.hmset(redisKey, map);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
    }

    public List<String> hget(String redisKey, String... hkey) {
        Jedis jedis = null;
        List<String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.hmget(redisKey, hkey);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }

    public Map<String, String> hgetAll(String redisKey) {
        Jedis jedis = null;
        Map<String, String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.hgetAll(redisKey);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }

    public List<String> lget(String redisKey, Long start, Long end) {
        Jedis jedis = null;
        List<String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.lrange(redisKey, start, end);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;

    }

    public Set<String> zget(String redisKey, Long start, Long end) {
        Jedis jedis = null;
        Set<String> res = null;
        try {
            jedis = jedisPool.getResource();
            res = jedis.zrange(redisKey, start, end);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            close(jedis);
        }
        return res;
    }


}
