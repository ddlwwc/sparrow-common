package xin.sparrow.cache.redis;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import xin.sparrow.cache.bean.RedisConfig;


/**
 * Created by wancheng on 2019/9/27.
 */
@Configuration
@EnableConfigurationProperties(RedisConfig.class)
public class RedisSource{

        @Bean
        public JedisPool createJedisSource(RedisConfig redisConfig) {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(redisConfig.getMaxTotal());
            jedisPoolConfig.setMaxIdle(redisConfig.getMaxIdle());
            jedisPoolConfig.setMaxWaitMillis(redisConfig.getMaxWait());
            jedisPoolConfig.setTestOnBorrow(redisConfig.isTestOnBorrow());
            jedisPoolConfig.setTestOnCreate(redisConfig.isTestOnCreate());
            jedisPoolConfig.setTestOnReturn(redisConfig.isTestOnReturn());
            jedisPoolConfig.setTestWhileIdle(redisConfig.isTestWhileIdle());
            return new JedisPool(jedisPoolConfig,
                    redisConfig.getHost(),
                    redisConfig.getPort(),
                    redisConfig.getTimeout(),
                    redisConfig.getPassword(),
                    redisConfig.getDatabase());
        }

}
