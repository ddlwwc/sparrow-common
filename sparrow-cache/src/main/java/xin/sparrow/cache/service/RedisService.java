package xin.sparrow.cache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xin.sparrow.cache.annotion.RedisTransactional;
import xin.sparrow.cache.redis.RedisBase;

/**
 * Created by wancheng on 2019/10/12.
 */
@Service
public class RedisService {

    @Autowired
    RedisBase redisBase;

    @RedisTransactional
    public Object redisTranscationTest() throws Exception {
        redisBase.transactional();
        return redisBase.get("事物测试");
    }
}
