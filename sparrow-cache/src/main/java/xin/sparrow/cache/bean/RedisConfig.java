package xin.sparrow.cache.bean;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import redis.clients.jedis.JedisPool;

/**
 * Created by wancheng on 2019/9/27.
 */
@Data
@Accessors(chain = true)
@Slf4j
@ConfigurationProperties(
        prefix = "sparrow.redis"
)
public class RedisConfig {

    /**
     * ip 默认本机地址
     */
    private String host = "127.0.0.1";
    /**
     * 端口 默认6379
     */
    private int port = 6379;
    /**
     * 最大链接数
     */
    private int maxTotal = 2048;
    /**
     * 最小链接数
     */
    private int maxIdle = 200;
    /**
     * 等待时间
     */
    private int maxWait = 10000;
    /**
     * 数据库 默认0
     */
    private int database = 0;
    /**
     * 超时时间
     */
    private int timeout = 2000;
    /**
     * 密码
     */
    private String password = null;
    /**
     *
     */
    private boolean testOnCreate = false;
    /**
     *
     */
    private boolean testOnBorrow = false;
    /**
     *
     */
    private boolean testOnReturn = false;
    /**
     *
     */
    private boolean testWhileIdle = false;


}
