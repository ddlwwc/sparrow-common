package xin.sparrow.cache.annotion.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;
import xin.sparrow.cache.annotion.RedisTransactional;
import org.aspectj.lang.annotation.Around;
import xin.sparrow.cache.redis.RedisBase;

/**
 * @Author: wangwnahceng
 * @Description:
 * @Date: 16:37 2017/12/6
 */
@Aspect
@Slf4j
@Component
public class RedisTransactionAspect {

    @Autowired
    RedisBase redisBase;


    @Around("@annotation(redisTransactional)")
    public Object cached(final ProceedingJoinPoint pjp, RedisTransactional redisTransactional) throws Throwable {

        Object o = pjp.proceed(pjp.getArgs());
        return "aop返回测试";

    }



}
