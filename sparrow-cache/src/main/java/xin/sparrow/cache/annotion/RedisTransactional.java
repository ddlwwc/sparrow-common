package xin.sparrow.cache.annotion;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by wancheng on 2019/10/12.
 */
@Retention(RUNTIME)
@Target(METHOD)
@Documented
public @interface RedisTransactional {

    /**
     * 回滚异常类
     * @return
     */

    Class<? extends Throwable>[] rollbackFor() default {};

    /**
     * 不会滚异常类
     * @return
     */
    Class<? extends Throwable>[] noRollbackFor() default {};
}
