package sparrow.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xin.sparrow.cache.SparrowApplication;
import xin.sparrow.cache.service.RedisService;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SparrowApplication.class)
public class SparrowApplicationTests {

	@Autowired
	RedisService redisService;


	@Test
	public void contextLoads() throws Exception {
		redisService.redisTranscationTest();
	}

}
