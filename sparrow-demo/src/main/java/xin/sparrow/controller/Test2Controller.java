package xin.sparrow.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xin.sparrow.cache.service.RedisService;
import xin.sparrow.log.annotation.WebLog;
import xin.sparrow.cache.redis.RedisBase;
import xin.sparrow.service.TestService;

/**
 * Created by wancheng on 2019/10/10.
 */
@Controller
@Slf4j
public class Test2Controller {

    @Autowired
    RedisService redisService;
    @Autowired
    TestService testService;

    @ResponseBody
    @GetMapping("/test2")
    @WebLog(name = "简单的测试方法")
    public JSONObject test() throws Exception {

        JSONObject res = new JSONObject();
        res.put("sss",redisService.redisTranscationTest());
        return res;
    }


    @GetMapping("/demo2")
    @WebLog(name = "demo页面测试")
    public String demo(){
      //  redisBase.set("demo","demo"+System.currentTimeMillis());
        return "demo.html";
    }

    @GetMapping("/test3")
    @WebLog(name = "demo页面测试")
    public String test3(String userName){
        testService.test();
        return "demo.html";
    }
}
