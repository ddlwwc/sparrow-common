package xin.sparrow.bean;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by wancheng on 2019/4/3.
 */
@Slf4j
@Data
@Accessors(chain = true)
public class ConstructorParams {
    private Class<?> paramClazz;
    private Object paramValue;

    public ConstructorParams(Class<?> paramClazz, Object paramValue) {
        this.paramClazz = paramClazz;
        this.paramValue = paramValue;
    }

}
