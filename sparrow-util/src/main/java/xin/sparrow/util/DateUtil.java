package xin.sparrow.util;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author wancheng  on 2018/12/20.
 */
public class DateUtil {

    public final static String DEFAULT_FORMAT_PATTERN_0 = "yyyy-MM-dd HH:mm:ss";
    public final static String DEFAULT_FORMAT_PATTERN_1 = "yyyy.MM.dd";
    public final static String DEFAULT_FORMAT_PATTERN_2 = "HH:mm";
    public final static String DEFAULT_FORMAT_PATTERN_3 = "yyyy-MM-dd HH:mm";
    public final static String DEFAULT_FORMAT_PATTERN_4 = "HH:mm:ss";
    public final static String DEFAULT_FORMAT_PATTERN_5 = "MM月dd日";
    public final static String DEFAULT_FORMAT_PATTERN_YEAR = "yyyy";
    public final static String DEFAULT_FORMAT_PATTERN_MONTH = "MM";
    public final static String DEFAULT_FORMAT_PATTERN_DAY = "dd";
    public final static String DEFAULT_FORMAT_PATTERN_HOUR = "HH";
    public final static String DEFAULT_FORMAT_PATTERN_MINUTE = "mm";
    public final static String DEFAULT_FORMAT_PATTERN_SECOND = "ss";
    public final static Integer VALUE_MINUTE = 60;

    public static Integer caleMinute(Date before, Date after, Boolean ifReturnAbsoluteValue) {
        Date tmp1 = DateUtils.truncate(before, Calendar.MINUTE);
        Date tmp2 = DateUtils.truncate(after, Calendar.MINUTE);
        Long ret = (tmp2.getTime() - tmp1.getTime()) / 60000;
        ret = ifReturnAbsoluteValue ? Math.abs(ret) : ret;
        return ret.intValue();
    }

    public static Integer caleHour(Date before, Date after, Boolean ifReturnAbsoluteValue) {
        Date tmp1 = DateUtils.truncate(before, Calendar.HOUR);
        Date tmp2 = DateUtils.truncate(after, Calendar.HOUR);
        Long ret = (tmp2.getTime() - tmp1.getTime()) / 3600000;
        ret = ifReturnAbsoluteValue ? Math.abs(ret) : ret;
        return ret.intValue();
    }

    public static Date str2Date(String str, String pattern) {
        Date d = null;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            d = sdf.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    public static Date str2Date(String str) {
        return str2Date(str, DEFAULT_FORMAT_PATTERN_0);
    }

    public static String date2Str(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    public static String date2Str(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_FORMAT_PATTERN_0);
        return sdf.format(date);
    }
    public static Date addMonth(Date date, Integer months) {
        if (months == null){
            months = 0;
        }
        return DateUtils.addMonths(date, months);
    }
    public static Date addDay(Date date, Integer days) {
        if (days == null){
            days = 0;
        }
        return DateUtils.addDays(date, days);
    }

    public static Date addHour(Date date, Integer hours) {
        if (hours == null){
            hours = 0;
        }
        return DateUtils.addHours(date, hours);
    }

    public static Date addMinute(Date date, Integer minutes) {
        if (minutes == null){
            minutes = 0;
        }
        return DateUtils.addMinutes(date, minutes);
    }

    public static String getDurationString(Integer minutes) {
        return String.format("%s小时%s分", minutes / VALUE_MINUTE, minutes % VALUE_MINUTE);
    }
}
