package xin.sparrow.util;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by wancheng on 2018/9/5.
 */
public class StringUtil {

    public static String phoneNumberFormat(String phone) {
        if (StringUtils.isEmpty(phone)){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (phone.length() == 11){
            String reStr = phone.substring(phone.length() - 4, phone.length());
            String preStr = phone.substring(0, phone.length() - 8);
            sb.append(preStr).append("****").append(reStr);
        }else{
            for (int i = 0 ; i< phone.length()-3; i ++){
                sb.append("*");
            }
            String reStr = phone.substring(phone.length() - 4, phone.length());
            sb.append(reStr);
        }

        return sb.toString();
    }
}
