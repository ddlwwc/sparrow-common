package xin.sparrow.util;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by wancheng on 2019/5/17.
 */
public class ImageUtil {

    /**
     * 生成一张固定大小的huhx1.jpg
     * @param inUrl
     * @param outUrl
     */
    public static void thumbnailator1(String inUrl,String outUrl) {
        try {
            Thumbnails.of(inUrl).size(80, 80).toFile(outUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件夹所有图片都生成缩略图
     */
    public static void thumbnailator2() {
        try {
            Thumbnails.of(new File("image").listFiles()).size(640, 480).outputFormat("jpg").toFiles(Rename.PREFIX_DOT_THUMBNAIL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 旋转角度的缩略图
     */
    public static void thumbnailator3(String inUrl,String outUrl) {
        try {
            Thumbnails.of(new File(inUrl)).scale(1).rotate(90).outputFormat("jpg").toFile(outUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成一个带有旋转和水印的缩略图
     */
    public static void thumbnailator4() {
        try {
            Thumbnails.of(new File("image/huhx.jpg")).size(160, 160).rotate(90)
                    .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File("image/water.jpg")), 0.5f)
                    .outputQuality(0.8f).toFile(new File("photo/huhx3.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 把生成的图片输出到输出流
     */
    public static void thumbnailator5() {
        try {
            OutputStream os = new FileOutputStream("photo/huhx4.jpg");
            Thumbnails.of("image/huhx.jpg").size(200, 200).outputFormat("jpg").toOutputStream(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 按一定的比例生成缩略图，生成缩略图的大小是原来的25%
     */
    public static void thumbnailator6() {
        try {
            BufferedImage originalImage = ImageIO.read(new File("/Users/wancheng/Desktop/image/140638uxxwsxi350bk227b.jpg"));
            BufferedImage thumbnail = Thumbnails.of(originalImage).scale(0.25f).asBufferedImage();
            ImageIO.write(thumbnail, "JPG", new File("/Users/wancheng/Desktop/image/140638uxxwsxi350bk227b————1.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 对图片进行裁剪
     */
    public static void thumbnailator7() {
        try {
            /**
             * 中心400*400的区域
             */
            Thumbnails.of("/Users/wancheng/Desktop/image/140638uxxwsxi350bk227b.jpg").sourceRegion(Positions.CENTER, 800, 800)
                    .size(200, 200).keepAspectRatio(false).toFile("/Users/wancheng/Desktop/image/140638uxxwsxi350bk227b__2.jpg");
            /**
             * 右下400*400的区域

            Thumbnails.of("image/huhx.jpg").sourceRegion(Positions.BOTTOM_RIGHT, 400, 400).size(200, 200).keepAspectRatio(false).toFile("photo/huhxRight.jpg");
             */
            /**
             * 指定坐标(0, 0)和(400, 400)区域，再缩放为200*200
            Thumbnails.of("image/huhx.jpg").sourceRegion(0, 0, 400, 400).size(200, 200).keepAspectRatio(false).toFile("photo/huhxRegion.jpg");
             */
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args){

        String inUrl = "/Users/wancheng/Desktop/image/140638uxxwsxi350bk227b.jpg";
        String outUrl = "/Users/wancheng/Desktop/image/140638uxxwsxi350bk227b————1.jpg";
        thumbnailator7();
    }
}


