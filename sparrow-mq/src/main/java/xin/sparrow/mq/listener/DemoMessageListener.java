package xin.sparrow.mq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static xin.sparrow.mq.config.QueueConst.QUEUE_A;
import static xin.sparrow.mq.config.QueueConst.QUEUE_B;
import static xin.sparrow.mq.config.QueueConst.QUEUE_C;

/**
 * Created by wancheng on 2019/10/9.
 */
@Component
public class DemoMessageListener {

    @RabbitListener(queues = QUEUE_A)
    public void queueASay(String string) {
        System.out.println("我是" + QUEUE_A + "," + "String:" + string);
    }

    @RabbitListener(queues = QUEUE_B)
    public void queueBSay(Integer num) {
        System.out.println("我是" + QUEUE_B + "," + "Integer:" + num);
    }

    @RabbitListener(queues = QUEUE_C)
    public void queueCSay() {
        System.out.println("我是" + QUEUE_C );
    }
}
