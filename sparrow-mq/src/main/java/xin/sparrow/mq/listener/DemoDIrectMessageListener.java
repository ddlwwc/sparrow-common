package xin.sparrow.mq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static xin.sparrow.mq.config.QueueConst.*;

/**
 * Created by wancheng on 2019/10/9.
 */
@Component
public class DemoDIrectMessageListener {

    @RabbitListener(queues = DIRECT_QUEUE_A)
    public void queueASay(String string) {
        System.out.println("我是" + DIRECT_QUEUE_A + "," + "String:" + string);
    }

    @RabbitListener(queues = DIRECT_QUEUE_B)
    public void queueBSay(Integer num) {
        System.out.println("我是" + DIRECT_QUEUE_B + "," + "Integer:" + num);
    }

    @RabbitListener(queues = DIRECT_QUEUE_C)
    public void queueCSay() {
        System.out.println("我是" + DIRECT_QUEUE_C );
    }
}
