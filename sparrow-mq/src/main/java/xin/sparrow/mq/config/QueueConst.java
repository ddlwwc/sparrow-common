package xin.sparrow.mq.config;

/**
 * Created by wancheng on 2019/10/9.
 */
public class QueueConst {

    /**
     * 队列
     */
    public static final String EXCHANGE_FANOUT = "exchange_fanout";
    public static final String EXCHANGE_DIRECT = "exchange_direct";
    public static final String EXCHANGE_TOPIC = "exchange_topic";
    public static final String EXCHANGE_HEADERS = "exchange_headers";

    public static final String QUEUE_A = "queue_a";
    public static final String QUEUE_B = "queue_b";
    public static final String QUEUE_C = "queue_c";


    public static final String DIRECT_QUEUE_A = "direct_queue_a";
    public static final String DIRECT_QUEUE_B = "direct_queue_b";
    public static final String DIRECT_QUEUE_C = "direct_queue_c";

    public static final String TOPIC_QUEUE_A = "topic_queue_a";
    public static final String TOPIC_QUEUE_B = "topic_queue_b";
    public static final String TOPIC_QUEUE_C = "topic_queue_c";

    public static final String HEADERS_QUEUE_A = "headers_queue_a";
    public static final String HEADERS_QUEUE_B = "headers_queue_b";
    public static final String HEADERS_QUEUE_C = "headers_queue_c";

    public static final String ROUTING_KEY_A = "spring-boot-routingKey_AA";
    public static final String ROUTING_KEY_B = "spring-boot-routingKey_BB";
    public static final String ROUTING_KEY_C = "spring-boot-routingKey_CC";
}
