package xin.sparrow.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static xin.sparrow.mq.config.QueueConst.*;

/**
 * Created by wancheng on 2019/10/9.
 * fanout策略交换机
 */
@Configuration
public class HeadersExchangeAndBindingConfiguration {

    /**
     * 注入队列A实例
     *
     * @return
     */
    @Bean(HEADERS_QUEUE_A)
    public Queue initQueueA() {
        return new Queue(HEADERS_QUEUE_A);
    }

    /**
     * 注入队列B实例
     *
     * @return
     */
    @Bean(HEADERS_QUEUE_B)
    public Queue initQueueB() {
        return new Queue(HEADERS_QUEUE_B);
    }

    /**
     * 注入队列C实例
     *
     * @return
     */
    @Bean(HEADERS_QUEUE_C)
    public Queue initQueueC() {
        return new Queue(HEADERS_QUEUE_C);
    }

    /**
     * 注入fanout策略的exchange交换机实例
     *
     * @return
     */
    @Bean(EXCHANGE_HEADERS)
    HeadersExchange initHeadersExchange() {
        return new HeadersExchange(EXCHANGE_HEADERS);
    }

    /**
     * 将三个队列分别绑定到 fanout交换机上
     *
     * @param queue
     * @param directExchange
     * @return
     */
    @Bean
    Binding bindingQueueAtoTopicExchange(@Qualifier(HEADERS_QUEUE_A) Queue queue, @Qualifier(EXCHANGE_HEADERS) HeadersExchange headersExchange) {
        Map<String,Object> headers = new HashMap<>();
        headers.put("name","ddlwwc");
        headers.put("action","eat");
        return BindingBuilder.bind(queue).to(headersExchange).whereAny(headers).match();
    }

    @Bean
    Binding bindingQueueBtoTopicExchange(@Qualifier(HEADERS_QUEUE_B) Queue queue, @Qualifier(EXCHANGE_HEADERS) HeadersExchange headersExchange) {
        Map<String,Object> headers = new HashMap<>();
        headers.put("name","ddlwwc");
        headers.put("sex","play");
        return BindingBuilder.bind(queue).to(headersExchange).whereAny(headers).match();
    }


}
