package xin.sparrow.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static xin.sparrow.mq.config.QueueConst.*;

/**
 * Created by wancheng on 2019/10/9.
 * fanout策略交换机
 */
@Configuration
public class DirectExchangeAndBindingConfiguration {

    /**
     * 注入队列A实例
     *
     * @return
     */
    @Bean(DIRECT_QUEUE_A)
    public Queue initQueueA() {
        return new Queue(DIRECT_QUEUE_A);
    }

    /**
     * 注入队列B实例
     *
     * @return
     */
    @Bean(DIRECT_QUEUE_B)
    public Queue initQueueB() {
        return new Queue(DIRECT_QUEUE_B);
    }

    /**
     * 注入队列C实例
     *
     * @return
     */
    @Bean(DIRECT_QUEUE_C)
    public Queue initQueueC() {
        return new Queue(DIRECT_QUEUE_C);
    }

    /**
     * 注入fanout策略的exchange交换机实例
     *
     * @return
     */
    @Bean(EXCHANGE_DIRECT)
    DirectExchange initDirectExchange() {
        return new DirectExchange(EXCHANGE_DIRECT);
    }

    /**
     * 将三个队列分别绑定到 fanout交换机上
     *
     * @param queue
     * @param directExchange
     * @return
     */
    @Bean
    Binding bindingQueueAtoDirectExchange(@Qualifier(DIRECT_QUEUE_A) Queue queue, @Qualifier(EXCHANGE_DIRECT) DirectExchange directExchange) {
        return BindingBuilder.bind(queue).to(directExchange).with(ROUTING_KEY_A);
    }

    @Bean
    Binding bindingQueueBtoDirectExchange(@Qualifier(DIRECT_QUEUE_B) Queue queue, @Qualifier(EXCHANGE_DIRECT) DirectExchange directExchange) {
        return BindingBuilder.bind(queue).to(directExchange).with(ROUTING_KEY_B);
    }

    @Bean
    Binding bindingQueueCtoDirectExchange(@Qualifier(DIRECT_QUEUE_C) Queue queue, @Qualifier(EXCHANGE_DIRECT) DirectExchange directExchange) {
        return BindingBuilder.bind(queue).to(directExchange).with(ROUTING_KEY_C);
    }
}
