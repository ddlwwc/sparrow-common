package xin.sparrow.mq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static xin.sparrow.mq.config.QueueConst.*;

/**
 * Created by wancheng on 2019/10/9.
 * fanout策略交换机
 */
@Configuration
public class FanoutExchangeAndBindingConfiguration {

    /**
     * 注入队列A实例
     *
     * @return
     */
    @Bean(QUEUE_A)
    public Queue initQueueA() {
        return new Queue(QUEUE_A);
    }

    /**
     * 注入队列B实例
     *
     * @return
     */
    @Bean(QUEUE_B)
    public Queue initQueueB() {
        return new Queue(QUEUE_B);
    }

    /**
     * 注入队列C实例
     *
     * @return
     */
    @Bean(QUEUE_C)
    public Queue initQueueC() {
        return new Queue(QUEUE_C);
    }

    /**
     * 注入fanout策略的exchange交换机实例
     *
     * @return
     */
    @Bean(EXCHANGE_FANOUT)
    FanoutExchange initFanoutExchange() {
        return new FanoutExchange(EXCHANGE_FANOUT);
    }

    /**
     * 将三个队列分别绑定到 fanout交换机上
     *
     * @param queue
     * @param fanoutExchange
     * @return
     */
    @Bean
    Binding bindingQueueAtoFanoutExchange(@Qualifier(QUEUE_A) Queue queue, @Qualifier(EXCHANGE_FANOUT) FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queue).to(fanoutExchange);
    }

    @Bean
    Binding bindingQueueBtoFanoutExchange(@Qualifier(QUEUE_B) Queue queue, @Qualifier(EXCHANGE_FANOUT) FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queue).to(fanoutExchange);
    }

    @Bean
    Binding bindingQueueCtoFanoutExchange(@Qualifier(QUEUE_C) Queue queue, @Qualifier(EXCHANGE_FANOUT) FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queue).to(fanoutExchange);
    }
}
