package xin.sparrow.mq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static xin.sparrow.mq.config.QueueConst.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private AmqpTemplate amqpTemplate;

	@Test
	public void exchange_fanout_test() {
		amqpTemplate.convertAndSend(EXCHANGE_FANOUT,"",123);
	}

	@Test
	public void exchange_direct_test() {
		amqpTemplate.convertAndSend(EXCHANGE_DIRECT,ROUTING_KEY_C,123456);
	}
	@Test
	public void exchange_topic_test() {
		amqpTemplate.convertAndSend(EXCHANGE_TOPIC,"routing.xxx",123456);
	}

	@Test
	public void exchange_headers_test() {
		Map<String,Object> headers = new HashMap<>();
		headers.put("name","ddlwwc");
		headers.put("action","eat");

	}


}
