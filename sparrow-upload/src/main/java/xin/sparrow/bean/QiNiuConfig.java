package xin.sparrow.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by wancheng on 2019/10/6.
 */
@Component
public class QiNiuConfig {

    /*private static class LazyHolder {
        private static final QiNiuConfig OUR_INSTANCE = new QiNiuConfig();
    }

    public static QiNiuConfig getInstance() {
        return LazyHolder.OUR_INSTANCE;
    }

    private QiNiuConfig() {
    }*/

    /**
     * 七牛secretKey
     */
    private static String accessKey;

    /**
     * 七牛云secretKey
     */
    private static String secretKey;

    /**
     * 七牛云空间名
     */
    private static String bucket;

    @Value("${qiniu.accessKey}")
    public void setAccessKey(String accessKey) {
        QiNiuConfig.accessKey = accessKey;
    }
    @Value("${qiniu.secretKey}")
    public void setSecretKey(String secretKey) {
        QiNiuConfig.secretKey = secretKey;
    }
    @Value("${qiniu.bucket}")
    public void setBucket(String bucket) {
        QiNiuConfig.bucket = bucket;
    }

    public static String getAccessKey() {
        return accessKey;
    }

    public static String getSecretKey() {
        return secretKey;
    }

    public static String getBucket() {
        return bucket;
    }
}
