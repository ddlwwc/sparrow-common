package xin.sparrow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparrowUploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SparrowUploadApplication.class, args);
	}

}
