package xin.sparrow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xin.sparrow.bean.QiNiuConfig;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import xin.sparrow.manage.QiNiuUploadManage;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SparrowUploadApplicationTests {

    @Test
    public void contextLoads() {
        System.out.println(QiNiuUploadManage.simpleUploadToken());
    }

}
