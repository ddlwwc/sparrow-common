package xin.sparrow.dd.auth;



import xin.sparrow.dd.bean.DingTalkConfig;
import xin.sparrow.dd.util.DingTalkConstUtil;
import xin.sparrow.dd.util.DingTalkUtil;

import java.util.Arrays;

import static xin.sparrow.dd.auth.DingTalkJsApiSingnature.getJsApiSingnature;


/**
 * AccessToken和jsticket的获取封装
 */
public class AuthHelper {

    private static final String XI_NIAO_NONCESTR = "xiniao";

    private static final String[] JS_API_LIST = {
            "biz.contact.complexPicker",
            "biz.contact.choose",
            "biz.contact.chooseMobileContacts​",
            "biz.customContact.multipleChoose",
            "biz.customContact.choose",
            "biz.contact.departmentsPicker",
            "biz.contact.setRule",
            "biz.util.open",
            //语音鉴权api
            "device.audio.startRecord",
            "device.audio.stopRecord",
            "device.audio.onRecordEnd",
            "device.audio.download",
            "device.audio.play",
            "device.audio.pause",
            "device.audio.resume",
            "device.audio.stop",
            "device.audio.onPlayEnd",
            "device.audio.translateVoice"
    };

   public static DingTalkConfig getConfig(String urlString){

       Long timeStamp = System.currentTimeMillis();

       String jsTicket = DingTalkUtil.getJsapiTicket();

       String jsApiSingnature = getJsApiSingnature(urlString,XI_NIAO_NONCESTR,timeStamp,jsTicket);

       DingTalkConfig config = new DingTalkConfig();

       config.setAgentId(DingTalkConstUtil.XINIAO_AGENT_ID);

       config.setCorpId(DingTalkConstUtil.DINGTALK_CORPID);

       config.setNonceStr(XI_NIAO_NONCESTR);

       config.setTimeStamp(timeStamp);

       config.setSignature(jsApiSingnature);

       config.setUrl(urlString);

       config.setJsApiList(Arrays.asList(JS_API_LIST));

       return config;
   }

   public static void main(String[] args){
       String jsTicket = DingTalkUtil.getJsapiTicket();
       String jsApiSingnature = getJsApiSingnature("http://preinner.topschool.com/DingMIndex",XI_NIAO_NONCESTR,1536032733849L,jsTicket);
       System.out.print(jsApiSingnature);
   }
}
