package xin.sparrow.dd.util;

/**
 * @author wancheng
 */
public class DingTalkInstance {

    private static class LazyHolder {
        private static final DingTalkInstance OUR_INSTANCE = new DingTalkInstance();
    }

    public static DingTalkInstance getInstance() {
        return LazyHolder.OUR_INSTANCE;
    }
    private static String corpId;
    private static String corpSecret;
    private static String appkey;
    private static String appsecret;
    private static String agentId;
    private static String appLoginId;
    private static String appLoginSecret;

    public  String getAppkey() {
        return appkey;
    }

    public  void setAppkey(String appkey) {
        DingTalkInstance.appkey = appkey;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        DingTalkInstance.appsecret = appsecret;
    }

    public  String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        DingTalkInstance.agentId = agentId;
    }

    public  String getCorpId() {
        return corpId;
    }

    public  void setCorpId(String corpId) {
        DingTalkInstance.corpId = corpId;
    }

    public  String getCorpSecret() {
        return corpSecret;
    }

    public  void setCorpSecret(String corpSecret) {
        DingTalkInstance.corpSecret = corpSecret;
    }

    public  String getAppLoginId() {
        return appLoginId;
    }

    public  void setAppLoginId(String appLoginId) {
        DingTalkInstance.appLoginId = appLoginId;
    }

    public  String getAppLoginSecret() {
        return appLoginSecret;
    }

    public  void setAppLoginSecret(String appLoginSecret) {
        DingTalkInstance.appLoginSecret = appLoginSecret;
    }
}
