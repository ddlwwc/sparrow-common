package xin.sparrow.dd.util;

/**
 * @author wancheng  on 2018/7/17.
 */
public class DingTalkConstUtil {

    public static final String DINGTALK_APP_KEY = DingTalkInstance.getInstance().getAppkey();

    public static final String DINGTALK_APPSECRET = DingTalkInstance.getInstance().getAppsecret();

    public static final String DINGTALK_CORPID = DingTalkInstance.getInstance().getCorpId();

    public static final String XINIAO_AGENT_ID = DingTalkInstance.getInstance().getAgentId();

    public static final String DINGTALK_CORPSECRET = DingTalkInstance.getInstance().getCorpSecret();

    public static final String DINGTALK_APP_LOGIN_ID = DingTalkInstance.getInstance().getAppLoginId();

    public static final String DINGTALK_APP_LOGIN_SECRET = DingTalkInstance.getInstance().getAppLoginSecret();

    public static final String DINGTALK_CORPSECRET_YUPEI = "tLR22dT4q0Een9BTxmf61pBQpeeLuw9jEjrGDJdUMx2c-P56D-uqrAySNHdpsC2W";

    public static final String DINGTALK_GET_CODE_URL = "https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid="+DINGTALK_APP_LOGIN_ID+"&response_type=code&scope=snsapi_auth&state=STATE&redirect_uri=%s";

    public static final String GET = "GET";

    public static final String AGREE = "agree";

    public static final String USER_ID = "userId";

    public static final String PROCESS_INSTANCE_ID = "processInstanceId";

    public static final String UNIT = "unit";

    public static final String FROM_TIME = "fromTime";

    public static final String TO_TIME = "toTime";

    public static final String DURATION_IN_HOUR = "durationInHour";

    public static final String DETAIL_LIST = "detailList";

    public static final String DINGTALK_APP_TOKEN_URL = "https://oapi.dingtalk.com/gettoken";

    public static final String APP_GET_PERSISTENT_CODE = "https://oapi.dingtalk.com/sns/get_persistent_code";

    public static final String APP_GET_SNS_TOKEN = "https://oapi.dingtalk.com/sns/get_sns_token";

    public static final String APP_GET_USER_INFO = "https://oapi.dingtalk.com/sns/getuserinfo";

    public static final String SERVER_AUTH_SCOPES = "https://oapi.dingtalk.com/auth/scopes";

    public static final String SERVER_SEND_MSG = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2";

    public static final String SERVER_TOKEN_URL = "https://oapi.dingtalk.com/gettoken";

    public static final String SERVER_GET_USER_LIST = "https://oapi.dingtalk.com/user/list";

    public static final String APP_E_PART_GET_USER_INFO_URL = "https://oapi.dingtalk.com/user/getuserinfo";

    public static final String APP_E_PART_GET_USER_INFO_BY_CODE_URL = "https://oapi.dingtalk.com/sns/getuserinfo_bycode";

    public static final String SERVER_GET_JSAPI_TICKET = "https://oapi.dingtalk.com/get_jsapi_ticket";

    public static final String SERVER_USER_PROCESS_URL = "https://oapi.dingtalk.com/topapi/processinstance/listids";

    public static final String SERVER_USER_PROCESS_INSTANCE_URL = "https://oapi.dingtalk.com/topapi/processinstance/get";
}
