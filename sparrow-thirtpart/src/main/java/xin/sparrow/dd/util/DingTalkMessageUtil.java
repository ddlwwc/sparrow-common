package xin.sparrow.dd.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.taobao.api.ApiException;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import xin.sparrow.dd.bean.msg.*;

import static com.taobao.api.internal.tmc.MessageKind.Data;
import static xin.sparrow.dd.util.DingTalkConstUtil.SERVER_SEND_MSG;

/**
 * @author wancheng  on 2018/10/17.
 */
@Slf4j
public class DingTalkMessageUtil {

    public static final String SENDMST_TYPE_TEXT = "text";
    public static final String SENDMST_TYPE_LINK = "link";
    public static final String SENDMST_TYPE_ACTION_CARD = "action_card";
    public static final String SENDMST_TYPE_OA = "oa";
    public static final String SENDMST_TYPE_MARKDOWN = "markdown";

    private static DingTalkClient getClient(){
        DingTalkClient client = new DefaultDingTalkClient(SERVER_SEND_MSG);
        return client;
    }
    private static OapiMessageCorpconversationAsyncsendV2Request getRequest(String agentId, String userIds){
        OapiMessageCorpconversationAsyncsendV2Request request = new OapiMessageCorpconversationAsyncsendV2Request();
        request.setUseridList(userIds);
        request.setAgentId(Long.valueOf(agentId));
        request.setToAllUser(false);
        return request;
    }

    /**
     * 发送文本消息
     * @param bean
     * @return
     */
    public static Boolean sendTextMsg(DingTalkTextBean bean) {
        DingTalkClient client = getClient();
        OapiMessageCorpconversationAsyncsendV2Request request = getRequest(bean.getAgentId(),bean.getUserIds());
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        msg.setMsgtype(SENDMST_TYPE_TEXT);
        msg.setText(new OapiMessageCorpconversationAsyncsendV2Request.Text());
        msg.getText().setContent(bean.getContent());
        request.setMsg(msg);
        OapiMessageCorpconversationAsyncsendV2Response response = null;
        String accessToken = DingTalkUtil.getCorpAccessToken();
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        Boolean ifSendSuccess = false;
        if (response.getErrcode() == 0) {
            ifSendSuccess = true;
        }
        log.info("sendTextMsg,errorCode={},errorMsg={},DingTalkTextBean={},responseCode={},response={},task_id={}",
                response.getErrcode(),response.getErrmsg(), JSON.toJSONString(bean),response.getErrcode(),response.getErrmsg(),response.getTaskId());
        return ifSendSuccess;

    }
    public static Boolean sendLinkMsg(DingTalkLinkBean bean) {
        DingTalkClient client = getClient();
        OapiMessageCorpconversationAsyncsendV2Request request = getRequest(bean.getAgentId(),bean.getUserIds());
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        msg.setMsgtype(SENDMST_TYPE_LINK);
        msg.setLink(new OapiMessageCorpconversationAsyncsendV2Request.Link());
        msg.getLink().setTitle(bean.getTitle());
        msg.getLink().setText(bean.getText());
        msg.getLink().setMessageUrl(bean.getMessageUrl());
        msg.getLink().setPicUrl(bean.getPicUrl());
        request.setMsg(msg);
        OapiMessageCorpconversationAsyncsendV2Response response = null;
        String accessToken = DingTalkUtil.getCorpAccessToken();
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        Boolean ifSendSuccess = false;
        if (response.getErrcode() == 0) {
            ifSendSuccess = true;
        }
        log.info("sendLinkMsg errorCode={},errorMsg={},DingTalkLinkBean={},responseCode={},response={},task_id={}",
                response.getErrcode(),response.getErrmsg(), JSON.toJSONString(bean),response.getErrcode(),response.getErrmsg(),response.getTaskId());
        return ifSendSuccess;

    }

    public static Boolean sendActionCardMsg(DingTalkActionCardBean bean) {
        DingTalkClient client = getClient();
        OapiMessageCorpconversationAsyncsendV2Request request = getRequest(bean.getAgentId(),bean.getUserIds());
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        msg.setMsgtype(SENDMST_TYPE_ACTION_CARD);
        msg.setActionCard(new OapiMessageCorpconversationAsyncsendV2Request.ActionCard());
        msg.getActionCard().setTitle(bean.getTitle());
        msg.getActionCard().setMarkdown(bean.getMarkdown());
        msg.getActionCard().setSingleTitle(bean.getSingleTitle());
        msg.getActionCard().setSingleUrl(bean.getSingleUrl());
        msg.getActionCard().setBtnOrientation(bean.getBtnOrientation());
        msg.getActionCard().setBtnJsonList(bean.getBtnList());
        request.setMsg(msg);
        OapiMessageCorpconversationAsyncsendV2Response response = null;
        String accessToken = DingTalkUtil.getCorpAccessToken();
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        Boolean ifSendSuccess = false;
        if (response.getErrcode() == 0) {
            ifSendSuccess = true;
        }
        log.info("sendActionCardMsg errorCode={},errorMsg={},DingTalkActionCardBean={},responseCode={},response={},task_id={}",
                response.getErrcode(),response.getErrmsg(), JSON.toJSONString(bean),response.getErrcode(),response.getErrmsg(),response.getTaskId());
        return ifSendSuccess;

    }

    public static Boolean sendOaMsg(DingTalkOaBean bean) {
        DingTalkClient client = getClient();
        OapiMessageCorpconversationAsyncsendV2Request request = getRequest(bean.getAgentId(),bean.getUserIds());
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        msg.setOa(new OapiMessageCorpconversationAsyncsendV2Request.OA());
        msg.getOa().setMessageUrl(bean.getMessageUrl());
        msg.getOa().setPcMessageUrl(bean.getPcMessageUrl());
        msg.getOa().setHead(new OapiMessageCorpconversationAsyncsendV2Request.Head());
        msg.getOa().getHead().setBgcolor(bean.getHeadBgcolor());
        msg.getOa().getHead().setText(bean.getHeadText());
        msg.getOa().setBody(new OapiMessageCorpconversationAsyncsendV2Request.Body());
        msg.getOa().getBody().setTitle(bean.getBodyTitle());
        msg.getOa().getBody().setContent(bean.getBodyContent());
        msg.getOa().getBody().setImage(bean.getBodyImage());
        msg.getOa().getBody().setFileCount(bean.getBodyFileCount());
        msg.getOa().getBody().setAuthor(bean.getBodyAuthor());
        if (bean.getBodyForm() != null){
            msg.getOa().getBody().setForm(JSONObject.parseArray(bean.getBodyForm().toJSONString(),OapiMessageCorpconversationAsyncsendV2Request.Form.class));
        }
        if (StringUtils.isNotBlank(bean.getBodyRich())){
            msg.getOa().getBody().setRich(JSONObject.parseObject(bean.getBodyRich(),OapiMessageCorpconversationAsyncsendV2Request.Rich.class));
        }
        msg.setMsgtype(SENDMST_TYPE_OA);
        request.setMsg(msg);
        OapiMessageCorpconversationAsyncsendV2Response response = null;
        String accessToken = DingTalkUtil.getCorpAccessToken();
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        Boolean ifSendSuccess = false;
        if (response.getErrcode() == 0) {
            ifSendSuccess = true;
        }
        log.info("sendOaMsg,errorCode={},errorMsg={},DingTalkOaBean={},responseCode={},response={},task_id={}",
                response.getErrcode(),response.getErrmsg(), JSON.toJSONString(bean),response.getErrcode(),response.getErrmsg(),response.getTaskId());
        return ifSendSuccess;

    }
    public static Boolean sendMarkdownMsg(DingTalkMarkdownBean bean) {
        DingTalkClient client = getClient();
        OapiMessageCorpconversationAsyncsendV2Request request = getRequest(bean.getAgentId(),bean.getUserIds());
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        msg.setMsgtype(SENDMST_TYPE_MARKDOWN);
        msg.setMarkdown(new OapiMessageCorpconversationAsyncsendV2Request.Markdown());
        msg.getMarkdown().setText(bean.getText());
        msg.getMarkdown().setTitle(bean.getTitle());
        request.setMsg(msg);
        OapiMessageCorpconversationAsyncsendV2Response response = null;
        String accessToken = DingTalkUtil.getCorpAccessToken();
        try {
            response = client.execute(request, accessToken);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        Boolean ifSendSuccess = false;
        if (response.getErrcode() == 0) {
            ifSendSuccess = true;
        }
        log.info("sendOaMsg,errorCode={},errorMsg={},DingTalkOaBean={},responseCode={},response={},task_id={}",
                response.getErrcode(),response.getErrmsg(), JSON.toJSONString(bean),response.getErrcode(),response.getErrmsg(),response.getTaskId());
        return ifSendSuccess;

    }

}
