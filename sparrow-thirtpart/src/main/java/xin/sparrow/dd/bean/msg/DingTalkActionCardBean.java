package xin.sparrow.dd.bean.msg;


import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkActionCardBean extends DingTalkMsgBaseBean{

    private String title;

    private String markdown;

    private String singleTitle;

    private String singleUrl;

    private String btnOrientation;

    private List<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList> btnList;

    public String getBtnOrientation() {
        return btnOrientation;
    }

    public void setBtnOrientation(String btnOrientation) {
        this.btnOrientation = btnOrientation;
    }

    public List<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList> getBtnList() {
        return btnList;
    }

    public void setBtnList(List<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList> btnList) {
        this.btnList = btnList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMarkdown() {
        return markdown;
    }

    public void setMarkdown(String markdown) {
        this.markdown = markdown;
    }

    public String getSingleTitle() {
        return singleTitle;
    }

    public void setSingleTitle(String singleTitle) {
        this.singleTitle = singleTitle;
    }

    public String getSingleUrl() {
        return singleUrl;
    }

    public void setSingleUrl(String singleUrl) {
        this.singleUrl = singleUrl;
    }

    public void addBtn(String title,String url){
        if (this.btnList == null){
            this.btnList = new ArrayList<>();
        }
        OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList btn = new OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList();
        btn.setActionUrl(url);
        btn.setTitle(title);
        this.btnList.add(btn);
    }
}
