package xin.sparrow.dd.bean.msg;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkMarkdownBean extends DingTalkMsgBaseBean{

   private String text;

    private String title;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
