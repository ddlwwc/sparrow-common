package xin.sparrow.dd.bean.msg;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkSingleActionCardBean extends DingTalkMsgBaseBean{

    private String title;

    private String markdown;

    private String singleTitle;

    private String singleUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMarkdown() {
        return markdown;
    }

    public void setMarkdown(String markdown) {
        this.markdown = markdown;
    }

    public String getSingleTitle() {
        return singleTitle;
    }

    public void setSingleTitle(String singleTitle) {
        this.singleTitle = singleTitle;
    }

    public String getSingleUrl() {
        return singleUrl;
    }

    public void setSingleUrl(String singleUrl) {
        this.singleUrl = singleUrl;
    }
}
