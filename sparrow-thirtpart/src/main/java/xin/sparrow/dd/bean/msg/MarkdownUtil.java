package xin.sparrow.dd.bean.msg;


import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wancheng  on 2018/10/17.
 */
public class MarkdownUtil {
    /**
     * 标题
     # 一级标题
     ## 二级标题
     ### 三级标题
     #### 四级标题
     ##### 五级标题
     ###### 六级标题
     * @param title
     * @param level
     * @return
     */
    public static String getTitle(String title, Integer level) {
        String templet = "%s %s";
        StringBuffer sign = new StringBuffer("");
        for (int i = 0; i < level; i++) {
            sign.append("#");
        }
        return String.format(templet, sign.toString(), title);
    }

    /**
     * 引用
     > A man who stands for nothing will fall for anything.
     * @param content
     * @return
     */
    public static String getQuote(String content) {
        StringBuffer sign = new StringBuffer("> ");
        sign.append(content);
        return sign.toString();
    }

    /**
     * 文字加粗、斜体
     **bold**
     *italic*
     * @param content
     * @return
     */
    public static String getFontWeight(String content) {
        String templet = "%s%s%s";
        return String.format(templet, "**", content, "**");
    }
    public static String getFontItalic(String content) {
        String templet = "%s%s%s";
        return String.format(templet, "*", content, "*");
    }
    public static String getString(String content) {
       return content;
    }

    /**
     * 链接
     [this is a link](http://name.com)
     * @param link
     * @param description
     * @return
     */
    public static String getLink(String link, String description) {
        StringBuffer sign = new StringBuffer("");
        sign.append("[");
        sign.append(description);
        sign.append("]");
        sign.append("(");
        sign.append(link);
        sign.append(")");
        return sign.toString();
    }

    /**
     * 图片
     ![](http://name.com/pic.jpg)
     * @param image
     * @return
     */
    public static String getImage(String image) {
        if (!StringUtils.isEmpty(image)){
            StringBuffer sign = new StringBuffer("");
            sign.append("![]");
            sign.append("(");
            sign.append(image);
            sign.append(")");
            return sign.toString();
        }
        return "";
    }

    /**
     *  无序列表
         - item1
         - item2

         有序列表
         1. item1
         2. item2
     * @param array
     * @param ifSort
     * @return
     */
    public static String getList(String[] array, Boolean ifSort) {
        List<String> list = Arrays.asList(array);
        return getList(list,ifSort);
    }
    public static String getList(List<String> list, Boolean ifSort) {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < list.size() ; i++) {
            StringBuffer sb = new StringBuffer("");
            if (ifSort){
                sb.append(i+1+". ");
            }else{
                sb.append("- ");
            }
            sb.append(list.get(i));
            res.add(sb.toString());
        }
        return res.stream().collect(Collectors.joining("\n"));
    }
    public static String getMarkdown(String... array){
        List<String> arrayList = Arrays.asList(array);
        return arrayList.stream().collect(Collectors.joining(" \n "));
    }
}
