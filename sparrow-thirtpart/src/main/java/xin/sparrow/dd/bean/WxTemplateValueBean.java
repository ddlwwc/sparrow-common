package xin.sparrow.dd.bean;

/**
 * 模版消息字段实体类
 * Created by jun on 2018/6/6.
 */
public class WxTemplateValueBean {
    private String value;
    private String color;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
