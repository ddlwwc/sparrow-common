package xin.sparrow.dd.bean;

import java.util.HashMap;

/**
 * 模版消息类
 * Created by jun on 2018/6/6.
 */
public class WxTemplateBean {
    private String touser;
    private String template_id;
    private String url;
    private HashMap<String, WxTemplateValueBean> data = new HashMap<>();

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String templateId) {
        template_id = templateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, WxTemplateValueBean> getData() {
        return data;
    }

    public void setData(HashMap<String, WxTemplateValueBean> data) {
        this.data = data;
    }

    public void addItem(String key, String value, String color) {
        WxTemplateValueBean v = new WxTemplateValueBean();
        v.setValue(value);
        v.setColor(color);
        data.put(key, v);
    }
}
