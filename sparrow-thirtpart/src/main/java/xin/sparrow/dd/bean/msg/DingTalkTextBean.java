package xin.sparrow.dd.bean.msg;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkTextBean extends DingTalkMsgBaseBean{

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
