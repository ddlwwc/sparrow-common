package xin.sparrow.dd.bean.msg;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkOaBean extends DingTalkMsgBaseBean{

    /**
     *消息点击链接地址，当发送消息为小程序时支持小程序跳转链接
     */
    private String messageUrl;
    /**
     *PC端点击消息时跳转到的地址
     */
    private String pcMessageUrl;
    /**
     *
     消息头部的背景颜色。长度限制为8个英文字符，其中前2为表示透明度，后6位表示颜色值。不要添加0X
     */
    private String headBgcolor = "FFBBBBBB";
    /**
     *消息的头部标题（向普通会话发送时有效，向企业会话发送时会被替换为微应用的名字），长度限制为最多10个字符
     */
    private String headText;
    /**
     *消息体的标题，建议50个字符以内
     */
    private String bodyTitle;
    /**
     *消息体的表单，最多显示6个，超过会被隐藏
     */
    private JSONArray bodyForm;
    /**
     *单行富文本信息
     */
    private String bodyRich;
    /**
     *消息体的内容，最多显示3行
     */
    private String bodyContent;
    /**
     *消息体中的图片，支持图片资源@mediaId
     */
    private String bodyImage;
    /**
     *自定义的附件数目。此数字仅供显示，钉钉不作验证
     */
    private String bodyFileCount;
    /**
     *
     自定义的作者名字
     */
    private String bodyAuthor = "子曰青龙";

    public DingTalkOaBean setForm(String key,String value){
        if (this.bodyForm == null){
            this.bodyForm = new JSONArray();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key",key);
        jsonObject.put("value",value);
        this.bodyForm.add(jsonObject);
        return this;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getPcMessageUrl() {
        return pcMessageUrl;
    }

    public void setPcMessageUrl(String pcMessageUrl) {
        this.pcMessageUrl = pcMessageUrl;
    }

    public String getHeadBgcolor() {
        return headBgcolor;
    }

    public void setHeadBgcolor(String headBgcolor) {
        this.headBgcolor = headBgcolor;
    }

    public String getHeadText() {
        return headText;
    }

    public void setHeadText(String headText) {
        this.headText = headText;
    }

    public String getBodyTitle() {
        return bodyTitle;
    }

    public void setBodyTitle(String bodyTitle) {
        this.bodyTitle = bodyTitle;
    }

    public JSONArray getBodyForm() {
        return bodyForm;
    }

    public void setBodyForm(JSONArray bodyForm) {
        this.bodyForm = bodyForm;
    }

    public String getBodyRich() {
        return bodyRich;
    }

    public void setBodyRich(String bodyRich) {
        this.bodyRich = bodyRich;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public String getBodyImage() {
        return bodyImage;
    }

    public void setBodyImage(String bodyImage) {
        this.bodyImage = bodyImage;
    }

    public String getBodyFileCount() {
        return bodyFileCount;
    }

    public void setBodyFileCount(String bodyFileCount) {
        this.bodyFileCount = bodyFileCount;
    }

    public String getBodyAuthor() {
        return bodyAuthor;
    }

    public void setBodyAuthor(String bodyAuthor) {
        this.bodyAuthor = bodyAuthor;
    }
}
