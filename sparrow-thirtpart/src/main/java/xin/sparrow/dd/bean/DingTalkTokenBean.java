package xin.sparrow.dd.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jun on 2018/7/30.
 */
public class DingTalkTokenBean implements Serializable {
    private String token;
    private Date validTime;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getValidTime() {
        return validTime;
    }

    public void setValidTime(Date validTime) {
        this.validTime = validTime;
    }
}
