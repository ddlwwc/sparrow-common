package xin.sparrow.dd.bean.msg;

import xin.sparrow.dd.util.DingTalkConstUtil;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkMsgBaseBean {
    String agentId = DingTalkConstUtil.XINIAO_AGENT_ID;
    String userIds;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }
}
