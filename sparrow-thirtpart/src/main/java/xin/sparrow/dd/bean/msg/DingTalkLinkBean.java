package xin.sparrow.dd.bean.msg;

/**
 * @author wancheng  on 2018/10/17.
 */
public class DingTalkLinkBean extends DingTalkMsgBaseBean{

    private String title;

    private String text;

    private String messageUrl;

    private String picUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
