package xin.sparrow.log.handler.bean;


import xin.sparrow.log.enums.HttpCodeEnum;

/**
 * Created by wancheng on 2018/12/23.
 * 参数校验异常返回信息实体类
 */
public class ExceptionResponseData {
    /**
     * 返回编码
     */
    private Integer code;

    private HttpCodeEnum codeMsg;
    /**
     * 访问的路径
     */
    private String url;
    /**
     * 返回信息
     */
    private String message;

    public ExceptionResponseData() {
    }

    public ExceptionResponseData(HttpCodeEnum codeMsg, String url, String message) {
        this.codeMsg = codeMsg;
        this.url = url;
        this.message = message;
        this.code = codeMsg.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public ExceptionResponseData setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public ExceptionResponseData setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ExceptionResponseData setMessage(String message) {
        this.message = message;
        return this;
    }

    public HttpCodeEnum getCodeMsg() {
        return codeMsg;
    }

    public void setCodeMsg(HttpCodeEnum codeMsg) {
        this.codeMsg = codeMsg;
    }
}
