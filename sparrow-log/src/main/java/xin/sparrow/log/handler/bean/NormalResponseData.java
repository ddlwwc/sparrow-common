package xin.sparrow.log.handler.bean;


import xin.sparrow.log.enums.HttpCodeEnum;

/**
 * Created by wancheng on 2019/3/26.
 */
public class NormalResponseData{

    private Integer code;
    private HttpCodeEnum codeMsg;
    private String description;
    private Object data;

    public NormalResponseData(HttpCodeEnum codeMsg, String description, Object data) {
        this.codeMsg = codeMsg;
        this.description = description;
        this.data = data;
        this.code = codeMsg.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public HttpCodeEnum getCodeMsg() {
        return codeMsg;
    }

    public void setCodeMsg(HttpCodeEnum codeMsg) {
        this.codeMsg = codeMsg;
    }
}
