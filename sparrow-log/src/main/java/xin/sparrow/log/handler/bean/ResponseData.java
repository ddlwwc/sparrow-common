package xin.sparrow.log.handler.bean;

import org.slf4j.Logger;

/**
 * Created by wancheng on 2019/3/26.
 */
public class ResponseData {

    private Logger logger;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
