package xin.sparrow.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparrowLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SparrowLogApplication.class, args);
	}

}
