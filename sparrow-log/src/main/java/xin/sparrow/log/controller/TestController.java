package xin.sparrow.log.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xin.sparrow.log.annotation.WebLog;

/**
 * Created by wancheng on 2019/10/10.
 */
@Controller
@Slf4j
public class TestController {

    @ResponseBody
    @GetMapping("/test")
    @WebLog(name = "简单的测试方法")
    public JSONObject test() throws Exception {

        JSONObject res = new JSONObject();
        res.put("sss","sss");
        if (true){
            throw new Exception("测试");
        }
        return res;
    }


    @GetMapping("/demo")
    @WebLog(name = "demo页面测试")
    public String demo() throws Exception {

        if (true){
            throw new Exception("测试");
        }
        return "demo.html";
    }
}
