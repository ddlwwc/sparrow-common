package xin.sparrow.log.annotation;

import java.lang.annotation.*;

/**
 * Created by wancheng on 2019/10/10.
 * controller层的日志注解
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebLog {

    /**
     * 方法名
     * @return
     */
    String desc() default "";

    /**
     * 名称
     * @return
     */
    String name() default "";

    /**
     *
     * @return
     */
    String[] value() default {};


}
