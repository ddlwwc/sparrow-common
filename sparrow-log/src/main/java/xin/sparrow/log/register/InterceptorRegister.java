package xin.sparrow.log.register;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import xin.sparrow.log.interceptor.ControllerLogInterceptor;

/**
 * Created by wancheng on 2019/10/11.
 */
@Configuration
public class InterceptorRegister extends WebMvcConfigurerAdapter {

    @Bean
    public ControllerLogInterceptor loggerInterceptor(){
        return new ControllerLogInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(loggerInterceptor());
    }
}
