package xin.sparrow.log.request;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created by wancheng on 2019/10/11.
 */
public class RequestThreadLocal {

    private static ThreadLocal<RequestAndResponseLog> requestContextHolder = new ThreadLocal<>();


    public static RequestAndResponseLog  set(){
        RequestAndResponseLog requestAndResponseLog = RequestResolution.initRequestLog();
        set(requestAndResponseLog);
        return requestAndResponseLog;
    }

    public static void  set(RequestAndResponseLog requestAndResponseLog){
        requestContextHolder.set(requestAndResponseLog);
    }

    public static RequestAndResponseLog  get(){
        return requestContextHolder.get();
    }

    public static void  remove(){
        requestContextHolder.remove();
    }
}
