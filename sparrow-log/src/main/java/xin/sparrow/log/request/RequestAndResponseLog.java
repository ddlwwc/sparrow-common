package xin.sparrow.log.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by wancheng on 2019/10/11.
 * 日志信息类
 */
@Data
@Accessors(chain = true)
public class RequestAndResponseLog {

    /**
     * 记录日志时间
     */
    private Date time;
    /**
     * 日志级别
     */
    private String level;
    /**
     * 日志信息
     */
    private String logBody;
    /**
     * 线程信息
     */
    private String thread;
    /**
     * 接口的简要说明
     */
    private String desc;
    /**
     * 请求uri
     */
    private String uri;
    /**
     * 请求参数
     */
    private String reqParam;
    /**
     * 返回的状态码
     */
    private Integer code;
    /**
     * 返回类型
     */
    private String responseType;
    /**
     * 返回结果
     */
    private String response;
    /**
     * 响应时间
     */
    private Long responseTime;
    /**
     * session信息
     */
    private String session;
    /**
     * 请求的ip
     */
    private String ip;
    /**
     * 设备信息
     */
    private String agent;
    /**
     * 相关类
     */
    private String classMsg;

    /**
     * 相关方法
     */
    private String method;

    /**
     * 抛出的异常
     */
    private Exception exception;


}
