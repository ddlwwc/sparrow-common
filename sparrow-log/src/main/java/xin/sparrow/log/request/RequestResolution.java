package xin.sparrow.log.request;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Map;

/**
 * Created by wancheng on 2019/10/11.
 */
public class RequestResolution {

    /**
     * 获取当前线程的request
     * @return
     */
    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取当前线程的response
     * @return
     */
    public static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    public static RequestAndResponseLog initRequestLog(){
        HttpServletRequest request = getRequest();
        String ip = getRequestIp(request);
        String reqUri = request.getRequestURI();
        String reqUrl = request.getRequestURL().toString();
        String reqParams = JSONObject.toJSONString(request.getParameterMap());
        String reqMethod = request.getMethod();
        HttpSession httpSession = request.getSession();
        String userAgent = request.getHeader("User-Agent");
        String referrer = request.getHeader("Referer");
        RequestAndResponseLog requestLog = new RequestAndResponseLog();
        requestLog
                .setIp(ip)
                .setUri(reqUri)
                .setReqParam(reqParams)
                .setMethod(reqMethod)
                .setAgent(userAgent)
                .setSession(httpSession.getId());
        return requestLog;
    }
    public static String getRequestParam(HttpServletRequest request){
        return StringUtils.isNotBlank(request.getQueryString()) ? request.getQueryString() : "";
    }
    /**
     * 获取requestIp
     * @param request
     * @return
     */
    public static String getRequestIp(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if ("127.0.0.1".equals(ipAddress) || "0:0:0:0:0:0:0:1".equals(ipAddress)) {
                InetAddress ine = null;

                try {
                    ine = InetAddress.getLocalHost();
                } catch (UnknownHostException var4) {

                }

                ipAddress = ine.getHostAddress();
            }
        }

        if (ipAddress != null && ipAddress.length() > 15 && ipAddress.indexOf(",") > 0) {
            ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
        }

        return ipAddress;
    }
}
