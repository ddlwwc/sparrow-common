package xin.sparrow.log.enums;

/**
 * Created by wancheng on 2019/3/26.
 */
public enum HttpCodeEnum {
    /**
     * 成功状态吗
     */
    HTTP_SUCCESS(200,"成功"),
    HTTP_EXCEPTION_ERROR_CODE(501,"系统内部异常"),
    HTTP_BIND_ERROR_CODE(503,"校验参数异常");

    /**
     * 返回状态码
     */
    private Integer code;
    /**
     * 描述
     */
    private String desc;

    HttpCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "HttpCodeEnum{" +
                "code=" + code +
                ", desc='" + desc + '\'' +
                '}';
    }
}
