package xin.sparrow.log;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.MarkerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SparrowLogApplicationTests {

	@Test
	public void contextLoads() {
		log.info(MarkerFactory.getMarker("DB"),"hello,logback!");
	}

}
