package xin.sparrow.elasticsearch.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import xin.sparrow.elasticsearch.entity.Item;
import xin.sparrow.elasticsearch.service.ItemService;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SparrowElasticSearchApplicationTests {

	@Autowired
	ElasticsearchTemplate elasticsearchTemplate;
	@Autowired
	ItemService itemService;

	@Test
	public void contextLoads() {
		elasticsearchTemplate.createIndex(Item.class);
	}
	@Test
	public void contextLoads2() {
		Item item = new Item();
		item.setBrand("2222");
		item.setCategory("2222");
		item.setId(222l);
		item.setImages("2222");
		item.setPrice(2222d);
		item.setTitle("2222");
		itemService.insert(item);
	}
	@Test
	public void contextLoads3() {

		List<Item>  list = itemService.list("1111");
		System.out.print(list.toString());
	}

}
