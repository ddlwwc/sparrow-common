package xin.sparrow.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparrowElasticSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SparrowElasticSearchApplication.class, args);
	}

}
