package xin.sparrow.elasticsearch.dao;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import xin.sparrow.elasticsearch.entity.Item;

/**
 * Created by wancheng on 2019/10/19.
 */
public interface ItemDao extends ElasticsearchRepository<Item,Long> {
}
