package xin.sparrow.elasticsearch.service.impl;

import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xin.sparrow.elasticsearch.dao.ItemDao;
import xin.sparrow.elasticsearch.entity.Item;
import xin.sparrow.elasticsearch.service.ItemService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by wancheng on 2019/10/19.
 */
@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemDao itemDao;

    @Override
    public void insert(Item item) {
        itemDao.save(item);
    }

    @Override
    public List<Item> list(String search) {
        QueryStringQueryBuilder builder = new QueryStringQueryBuilder("id="+search);
        Iterable<Item> searchResult = itemDao.search(builder);
        Iterator<Item> itemIterator = searchResult.iterator();
        List<Item> items = new ArrayList<>();
        while (itemIterator.hasNext()){
            items.add(itemIterator.next());
        }
        return items;
    }
}
