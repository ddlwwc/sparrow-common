package xin.sparrow.elasticsearch.service;

import xin.sparrow.elasticsearch.entity.Item;

import java.util.List;

/**
 * Created by wancheng on 2019/10/19.
 */
public interface ItemService {

    void insert(Item item);

    List<Item> list(String search);
}
