package xin.sparrow.thread.demo2;

/**
 * Created by wancheng on 2019/10/17.
 */
public class SyncObjectThread2 extends Thread{

    private SyncObject syncObject;

    public SyncObjectThread2(SyncObject syncObject) {
        this.syncObject = syncObject;
    }

    public SyncObjectThread2(String name) {
        super();
        this.setName(name);
    }
    @Override
    public void run() {
        syncObject.dontSay();
    }

}
