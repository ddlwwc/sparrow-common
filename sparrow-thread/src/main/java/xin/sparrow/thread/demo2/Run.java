package xin.sparrow.thread.demo2;

/**
 * Created by wancheng on 2019/10/17.
 */
public class Run {

    public static void main(String[] args){
        SyncObject syncObject = new SyncObject("JJ");
        Thread t1 = new SyncObjectThread(syncObject);
        Thread t2 = new SyncObjectThread2(syncObject);
        t1.start();
        t2.start();
    }
}
