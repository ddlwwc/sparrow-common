package xin.sparrow.thread.demo2;

/**
 * Created by wancheng on 2019/10/17.
 */
public class SyncObject {

    private String name;

    public SyncObject(String name) {
        this.name = name;
    }

    public synchronized void say(String say){

        System.out.println("。。。等待5秒");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("我的名字叫:"+name+",我想说"+say);
        System.out.println("说完了");
    }
    public void dontSay(){
        System.out.println("我的名字叫:"+name+",哼。。。");
    }
}
