package xin.sparrow.thread.demo1;

/**
 * Created by wancheng on 2019/10/16.
 */
public class MyThread extends Thread{

    private int count = 5;

    public MyThread() {
    }

    public MyThread(String name) {
        super();
        this.setName(name);
    }
    @Override
    public void run() {
        super.run();
        while (count > 0){
            count --;
            System.out.println(" 由 " + currentThread().getName() + "计算.count=" + count);
        }
    }

    /*public static void main(String[] args){
        SyncObjectThread a = new SyncObjectThread("A");
        SyncObjectThread b = new SyncObjectThread("B");
        SyncObjectThread c = new SyncObjectThread("C");
        a.start();
        b.start();
        c.start();
    }*/
    public static void main(String[] args){
        MyThread myThread = new MyThread();
        Thread a = new Thread(myThread,"A");
        Thread b = new Thread(myThread,"B");
        Thread c = new Thread(myThread,"C");
        a.start();
        b.start();
        c.start();
    }
}
