package xin.sparrow.thread;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparrowThreadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SparrowThreadApplication.class, args);
	}

}
