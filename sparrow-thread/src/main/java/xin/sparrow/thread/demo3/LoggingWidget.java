package xin.sparrow.thread.demo3;

/**
 * Created by wancheng on 2019/10/22.
 */
public class LoggingWidget extends Widge{

    @Override
    public synchronized void doSomething(){
        System.out.println(toString() + ": calling doSomething");
        super.doSomething();
    }
}
