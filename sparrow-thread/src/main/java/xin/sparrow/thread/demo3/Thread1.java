package xin.sparrow.thread.demo3;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wancheng on 2019/10/22.
 */
public class Thread1 implements Callable{

    volatile int count = 0;

    private final int iterator_count = 10;

    @Override
    public Integer call() throws Exception {
        for (int i = 0; i < iterator_count ; i ++){
           count += i;
        }
        return count;
    }
}
