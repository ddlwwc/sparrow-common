package xin.sparrow.thread.demo3;

/**
 * Created by wancheng on 2019/10/22.
 */
public class LuckObject {

    int count = 0;

    public synchronized void setCount(){

        count ++;
        System.out.println(count);
        if (count <= 10){
            this.setCount();
        }
    }
}
