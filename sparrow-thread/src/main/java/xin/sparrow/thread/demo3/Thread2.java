package xin.sparrow.thread.demo3;

/**
 * Created by wancheng on 2019/10/22.
 */
public class Thread2 implements Runnable{

    private Son luckObject;

    public Thread2(Son luckObject) {
        this.luckObject = luckObject;
    }

    @Override
    public void run() {
        luckObject.setCount2();
    }
}
