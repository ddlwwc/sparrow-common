package xin.sparrow.thread.rbtree;

/**
 * Created by wancheng on 2019/10/23.
 */
public enum TreeColor {
    /**
     * 红黑树的颜色
     */
    RED,BLACK;
}
