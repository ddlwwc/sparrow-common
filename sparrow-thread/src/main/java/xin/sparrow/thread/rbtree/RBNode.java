package xin.sparrow.thread.rbtree;

/**
 * Created by wancheng on 2019/10/23.
 * 节点信息
 */
public class RBNode<T extends Comparable> {

    /**
     * 颜色
     */
    private Boolean color;

    /**
     * 关键值
     */
    private T key;

    /**
     * 左节点
     */
    private RBNode<T> left;

    /**
     * 右节点
     */
    private RBNode<T> right;

    /**
     * 父节点
     */
    private RBNode<T> parent;

    public Boolean getColor() {
        return color;
    }

    public void setColor(Boolean color) {
        this.color = color;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public RBNode<T> getLeft() {
        return left;
    }

    public void setLeft(RBNode<T> left) {
        this.left = left;
    }

    public RBNode<T> getRight() {
        return right;
    }

    public void setRight(RBNode<T> right) {
        this.right = right;
    }

    public RBNode<T> getParent() {
        return parent;
    }

    public void setParent(RBNode<T> parent) {
        this.parent = parent;
    }

    private void setBlack(RBNode<T> node) {
        this.color = false;
    }

    private void setRed(RBNode<T> node) {
        this.color = true;
    }

    public T getKey() {
        return key;
    }

    public String message() {
        return "值 :" + key + " 颜色 :" + (color ? "R" : "B");
    }


}
